Massey's Chem-Dry offers professional carpet and upholstery cleaning in Brunswick, Fernandina Beach, Kingsland and surrounding areas. We are dedicated to helping our customers maintain a clean, healthy, happy home through our proprietary process combined with our non-toxic, green-certified solution.

Website : http://www.masseyschemdry.com/
